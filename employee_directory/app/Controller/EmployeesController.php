<?php
class EmployeesController extends AppController{
    public $name='Employees';
    public  $helpers = array('Html','Form');  
    function index(){
        $data=  $this->Employee->find('all');
        $this->set('data',$data);
    }
    function edit($id = null){
        if (empty($this->data)){
            $this->Employee->id = $id;
            $this->data = $this->Employee->read();
        }
        else{
            if ($this->request->is('post')){
                $this->Employee->save($this->data['Employee']);
                $this->redirect('index');
                
            }
        }
    }
    function profile($id){
        $this->Employee->id=$id;
        $this->set('Employees',$this->Employee->read());
    }
//    function add(){
//        if (!empty($this->data)){
//            $this->Employee->create();
//            $filename = "app/webroot/files/img/".$this->data['Employee']['Photo']['name']; 
//            move_uploaded_file($this->data['Employee']['Photo']['tmp_name'],$filename);
//        if ($this->request->is('post') ) {
//            
//            $this->Employee->save($this->data);
//            $this->redirect( 'index');
//            } else {
//                $this->Session->setFlash(__('There was a problem. Please try again.'));
//            }
//     
//        }
//        }
       function add(){
        if (!empty($this->data)){
            $this->Employee->create();
            
            if ($this->Employee->save($this->data)){
                $filename = "app/webroot/files/img/".$this->data['Employee']['Photo']; 
                move_uploaded_file($this->data['Employee']['Photo'],$filename);
//                if($this->data['Employee']['Photo']){        
                $this->Photo="app/webroot/files/img/".$this->data['Employee']['Photo'];
//            }
                $this->redirect('index');
            }
            else{
               $this->setFlash(__('The Post could not be saved. Please try again',true));
       }
   }
   }
    function delete($id){
        $this->Employee->delete($id);
        $this->redirect('index');
    }
    
}
