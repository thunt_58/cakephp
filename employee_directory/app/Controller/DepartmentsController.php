<?php
class DepartmentsController extends AppController{
    public $name= 'Departments';
    public  $helpers = array('Html','Form');  
    function index(){
        $data = $this->Department->find("all");
        $this->set("data",$data);
    }
    function detail($id){
        $this->Department->id=$id;
        $this->set('Department',$this->Department->read());
    }
    function edit($id){
        if (empty($this->data)){
            $this->Department->id = $id;
            $this->data = $this->Department->read();
        }
        else{
            if ($this->request->is('post')){
                $this->Department->save($this->data['Department']);
                $this->redirect('index');
                
            }
        }
    }   
    function delete($id){
        $this->Department->delete($id);
        $this->redirect('index');
    }
    function add(){
        if (!empty($this->data)){
            $this->Department->create();
            if ($this->Department->save($this->data)){
                $this->redirect('index');
            }
            else{
                $this->setFlash(__('The Post could not be saved. Please try again',true));
            }
        }
    }
    
}
