<?php 
    echo $this->Form->create('Employee',array('action'=>'edit','type'=>'post'));
    echo $this->Form->input('Name');
    echo $this->Form->input('Photo',array('type'=>'file'));
    echo $this->Form->input('Department');
    echo $this->Form->input('Job_Title');
    echo $this->Form->input('Email');
    echo $this->Form->input('Cellphone');
    echo $this->Form->end('Update');
    
    echo $this->Html->link('Back','../Employees/index');
?>